#!/usr/bin/env bash

bundle install

calabash-android resign "../app/build/outputs/apk/debug/app-debug.apk"
calabash-android run "../app/build/outputs/apk/debug/app-debug.apk" -p app_test