@login
Feature: Login Feature

    Scenario: First time accessing the app
        Given the user opens app for the first time
        Then the login screen is displayed as per design

    Scenario: Login with Wrong Username or password
        Given I login with wrong detail
        Then I can see error marker

    Scenario: Login with valid Username or password
        Given I login with valid detail
        Then I can see success screen

   #Below scenario is not behaving as expected. Thats why I tagged it with @bug.
    @bug
    Scenario: Accessing app after login previously
        Given the user opens app after login previously
        Then I can see success screen




