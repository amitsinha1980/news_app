class PageBase < Calabash::ABase

# This method waits until the given control is displayed with maximum timeout as 20 seconds
# @param control [String] untill this is displayed
# @return [NilClass]
  def wait_until_this_displayed(control, timeout = 20)
    wait_poll(:until_exists => control, :timeout => timeout) do
    end
  end

  def check_if_element_exists?(element, timeout = 10)
    begin
      wait_until_this_displayed(element, timeout)
      return true
    rescue
      return false
    end
  end

  def check_if_element_does_not_exist?(element, timeout = 5)
    begin
      wait_until_this_displayed(element, timeout)
      return false
    rescue
      return true
    end
  end

  def touch_element(element)
    touch(element)
  end

  def query_an_element(element_locator)
    query(element_locator)
  end

  def get_text_property_of_an_element(element_query)
    query(element_query).first['text']
  end

    def flick_up(element_query)
        flick(element_query, :up)
    end

     def flick_down(element_query)
        flick(element_query, :down)
     end

      def flick_left(element_query)
         flick(element_query, :left)
      end

      def flick_left(element_query)
         flick(element_query, :right)
      end

  def wait_for_element_to_not_exist(element_query, timeout=5)
    wait_for_elements_do_not_exist(element_query, :timeout => timeout)
  end

  def wait_for_element_to_exist(element_query, timeout=5)
    wait_for_elements_exist(element_query, :timeout => timeout)
  end

  def enter_text_in_selected_field(text)
    keyboard_enter_text(text)
  end

  def dismiss_the_keyboard()
    press_user_action_button
  end

end
