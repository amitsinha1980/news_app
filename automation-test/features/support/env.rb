require 'calabash-android/cucumber'

require File.join(File.dirname(__FILE__), 'page_object')


require 'calabash-android'
require 'calabash-android/abase'
require 'calabash-android/helpers'
require 'rspec/expectations'

include RSpec::Matchers

World(PageObject)
