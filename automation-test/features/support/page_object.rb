module PageObject

  def login_screen()
    @login_screen ||= page(LoginScreen)
  end

    def news_listing_screen()
      @news_listing_screen ||= page(NewsListingScreen)
    end

end