Before('@disable_wifi') do
    puts "Disabling Wifi"
    cmd = "#{default_device.adb_command} shell svc wifi disable"
    system(cmd)
end

After('@disable_wifi') do
    puts "Enabling Wifi after Test run"
    cmd = "#{default_device.adb_command} shell svc wifi enable"
    system(cmd)
end