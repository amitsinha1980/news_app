class NewsListingScreen< PageBase

  def get_element_property
    element_hash = {
        "image_row" => "* id:'imageView'",
        "failed_to_load_error" => "* id:'textViewError'"
    }

    element_hash
  end

    def click_image
        @news_listing_screen_elements ||= get_element_property()
        wait_for_element_to_exist(@news_listing_screen_elements['image_row'])
        touch_element(query_an_element(@news_listing_screen_elements['image_row']).first)
    end

    def navigate_back_from_external_browser()
        cmd = "#{default_device.adb_command} shell input keyevent 4"
        system(cmd)
    end

    def get_failed_to_load_error_text
        @news_listing_screen_elements ||= get_element_property()
        wait_for_element_to_exist(@news_listing_screen_elements['failed_to_load_error'])
        get_text_property_of_an_element(@news_listing_screen_elements['failed_to_load_error'])
    end


end
