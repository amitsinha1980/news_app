class LoginScreen < PageBase

  def get_element_property
    element_hash = {
        "username" => "* id:'editTextUserName'",
        "password" => "* id:'editTextPassword'",
        "login_button" => "* id:'buttonLogin'",
        "Error_message" => 'androidx.appcompat.widget.AppCompatTextView'
    }

    element_hash
  end

  def enter_user_detail(username, password)
    @login_screen_elements = get_element_property()
    wait_for_element_to_exist(@login_screen_elements['username'])

    touch_element(query_an_element(@login_screen_elements['username'])[0])
    enter_text_in_selected_field(username)

    touch_element(query_an_element(@login_screen_elements['password'])[0])
    enter_text_in_selected_field(password)
  end

  def click_login_button()
    touch_element(query_an_element(@login_screen_elements['login_button']).first)
  end

  def check_element_exists(element)
    check_if_element_exists?(element)
  end

  def get_error_message()
    touch_element(query_an_element(@login_screen_elements['username'])[0])
    error_text = query(@login_screen_elements['Error_message'])[1]['text']
    error_text
  end


end
