
Then(/^images are displayed as per design$/) do
    news_listing_element = news_listing_screen.get_element_property
    expect(news_listing_screen.check_if_element_exists?(news_listing_element['image_row'])).to be_truthy, "Element news row not found"
    expect(news_listing_screen.query_an_element(news_listing_element['image_row']).count > 1 ).to be_truthy, "Only 1 row of image is displayed"

    news_listing_screen.flick_up(news_listing_element['image_row'])
    expect(news_listing_screen.check_if_element_exists?(news_listing_element['image_row'])).to be_truthy, "Element news row not found"

    news_listing_screen.flick_left(news_listing_element['image_row'])
    expect(news_listing_screen.check_if_element_exists?(news_listing_element['image_row'])).to be_truthy, "Element news row not found"

end

When(/^I click a new image$/) do
    news_listing_screen.click_image
end

Then(/^I can see message "(.*)"$/) do |error_text|
    actual_error = news_listing_screen.get_failed_to_load_error_text
    expect(actual_error.eql?(error_text)).to be_truthy, "Expected error #{error_text}... Actual #{actual_error}"
end

Then(/^I am taken to external browser$/) do
    news_listing_screen.navigate_back_from_external_browser
end