Given(/^the user opens app for the first time$/) do
    start_test_server_in_background
end

Given(/^the user opens app after login previously$/) do
    start_test_server_in_background
end

Then(/^the login screen is displayed as per design$/) do
    login_element = login_screen.get_element_property
    expect(login_screen.check_if_element_exists?(login_element['username'])).to be_truthy, "Element username not found"
    expect(login_screen.check_if_element_exists?(login_element['password'])).to be_truthy, "Element password not found"
    expect(login_screen.check_if_element_exists?(login_element['login_button'])).to be_truthy, "Element login button not found"
end

Given(/^I login with (.*) detail$/) do |login_type|
    case login_type
        when 'wrong'
            login_screen.enter_user_detail("Wrong_user", "Wrong_password")
        when 'valid'
            login_screen.enter_user_detail("user1", "password")
    end

    login_screen.click_login_button
end

Then(/^I can see error marker$/) do
    actual_error_text = login_screen.get_error_message
    expect(actual_error_text.eql?('Wrong user name'))
end

Then(/^I can see success screen$/) do
#     actual_error_text = login_screen.get_error_message
#     expect(actual_error_text.eql?('Wrong user name'))
end
