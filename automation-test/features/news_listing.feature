@news_listing
Feature: News Listing

  Background:
    Given I login with valid detail

  Scenario: New Image are Loaded
    Then images are displayed as per design

  #adb shell command to disable wifi seems to be not working on physical device. And on my mac emualtor is not working.
  #Thats why I tagged below test with @manul which will stop it from running. At present i cant verify if adb shell command works on emualtor
  @disable_wifi @manual
  Scenario: Login when there is no internet
    Then I can see message "Failed to load news"

  Scenario: Clicking a news image
    When I click a new image
    Then I am taken to external browser
